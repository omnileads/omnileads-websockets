# Omnileads Websockets Server

## CLI interfaces

The service provides several **CLI** interfaces they can be invoked differently, all are equivalents and depends on your setup or preference.

- **as poetry script** Usually for development time since we don't provide poetry in our prod environments.
- **as python module** For development time and production runtime.
- **as python script** For development time and production runtime.

All CLI args can be provided using ENV variables.

### websockets-server

Runs a websocket server, that listen for incoming connections and execute different routines

```sh
# poetry script
$ cd /path/to/websockets
$ poetry run fts-oml-services-websockets-server

# python module
$ /opt/fts-oml-services-websockets/.venv/bin/python -m fts.oml.services.websockets.server

# python script
$ /opt/fts-oml-services-websockets/.venv/bin/fts-oml-services-websockets-server
```

Example of environment vars:

```sh
# defaults
BIND_ADDRESS=0.0.0.0:8000
EVENT_LOOP=built-in
LOGGERS=DEBUG:fts.oml.services.websockets;INFO:websockets.server
REDIS_HOST=redis://redis:6379

# prod
EVENT_LOOP=uvloop
REDIS_HOST=redis+sentinel://redis/redis-sentinel-1,redis-sentinel-2,redis-sentinel-3

# devel
LOGGERS=DEBUG:fts.oml.services.websockets,websockets.server
```

### websockets-client

For now, just a skel. In the future will be a websocket client app for check/test all server features.

## HTTP/WS interfaces

### websockets-server

The service provides a HTTP/WS interface with several endpoints that define different routines.

`GET` `/<str:prefix>/health-checks`

Internal endpoint to be used by a parent load-balancer to check service health, or by a developer to debug the internal state.

```sh
curl -s http://127.0.0.1:64310/health-checks | jq
{
  "status": "pass",
  "checks": {
    "tasks": [
      "Task-1",
      ...
      "redis-pubsub id=605f58db-6bc8-4e9e-ab1e-88bf2b2b86d4 name=OML:AGENT_REPORT:CURRENT_DAY_STATS:1:dashboard1",
      "redis-stream id=9178f5fe-ac1f-4712-84b0-449672bae6b6 name=supervisor_1_agentes",
      "redis-stream id=f90eddc5-2fb6-4d6f-9791-17bb196396df name=asterisk_conf_updater"
    ],
    "redis:pubsubs": [
      "redis-pubsub id=605f58db-6bc8-4e9e-ab1e-88bf2b2b86d4 name=OML:AGENT_REPORT:CURRENT_DAY_STATS:1:dashboard1"
    ],
    "redis:streams": [
      "redis-stream id=9178f5fe-ac1f-4712-84b0-449672bae6b6 name=supervisor_1_agentes",
      "redis-stream id=f90eddc5-2fb6-4d6f-9791-17bb196396df name=asterisk_conf_updater"
    ],
    "websockets": [
      {
        "id": "9178f5fe-ac1f-4712-84b0-449672bae6b6",
        "path": "/consumers/stream/supervisor/1/agentes",
        "remote_address": "10.1.1.18:39420"
      },
      {
        "id": "f90eddc5-2fb6-4d6f-9791-17bb196396df",
        "path": "/consumers/stream/asterisk/conf/updater",
        "remote_address": "10.1.1.18:39438"
      },
      {
        "id": "605f58db-6bc8-4e9e-ab1e-88bf2b2b86d4",
        "path": "/consumers/reporte_agente/estadisticas_dia_actual/1/dashboard1",
        "remote_address": "10.1.1.18:39428"
      }
    ]
  },
  "releaseId": "0.1.0",
  "version": "0",
  "serviceId": "5d7025fe2dbd"
}
```

`WS` `/<str:prefix>/stream/<any:stream-name>`

Subscribe to a redis stream channel and send messages back to the connected client.

```sh
$ websocat ws://127.0.0.1:64310/consumers/stream/supervisor/1/agentes | jq
[
  "{\"value\": \"false\",'stream_id':'1651708042920-0'}",
  "{'STATUS': 'READY', ... 'NAME': 'Clarence Murray', ... 'stream_id':'1651708049805-0'}",
  "{'STATUS': 'OFFLINE', ... 'NAME': 'Clarence Murray', ... 'stream_id':'1651711683430-0'}"
]
```

`WS` `/<str:prefix>/<any:pubsub-name>`

Subscribe to a redis pubsub channel and send messages back to the connected client.

```sh
$ websocat ws://127.0.0.1:64310/consumers/reporte_agente/estadisticas_dia_actual/1/dashboard1
{
  "conectadas": "{\"total\": 0, \"salientes\": 0, \"entrantes\": 0}",
  "venta": "{\"total\": 0, \"observadas\": 0}",
  "logs": "[]",
  "tiempos": "{\"pausa\": 0.0, \"sesion\": -1345.338344, \"tiempo_pausa\": \"0:00:00 hs\"}",
  "pausas": "[]"
}
{
  "conectadas": "{\"total\": 0, \"salientes\": 0, \"entrantes\": 0}",
  "venta": "{\"total\": 0, \"observadas\": 0}",
  "logs": "[]",
  "tiempos": "{\"pausa\": 0.0, \"sesion\": -1286.470836, \"tiempo_pausa\": \"0:00:00 hs\"}",
  "pausas": "[]"
}
```

## Packages

### python

The python package is suitable for a direct install on a python virtualenv but the fact that we require `python=^3.10` and `hiredis` reduces the environments where we can do an install without compilation.

```sh
$ cd /path/to/websockets

$ poetry build
Building fts.oml.services.websockets (0.1.0)
  - Building sdist
  - Built fts.oml.services.websockets-0.1.0.tar.gz
  - Building wheel
  - Built fts.oml.services.websockets-0.1.0-py3-none-any.whl

$ ls -l dist
-rw-r--r-- ... fts.oml.services.websockets-0.1.0-py3-none-any.whl
-rw-r--r-- ... fts.oml.services.websockets-0.1.0.tar.gz
```

### container images

We provide container images for both **dev** and **run** to ease the development and execution of the service. We use the same `Dockerfile` with [multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/) and [BuildKit](https://docs.docker.com/develop/develop-images/build_enhancements/).

Those images are usually created by a ci pipeline but you can use `poetry run poe websockets:docker-build` for recreating the images with the local version of the project.

#### freetechsolutions/oml-websockets:dev

Image for development, that supports code auto reload on change. It includes additional tools, e.g. `direnv`, `poetry` and `watchexec`.

You can use the following **docker-compose** service definition. If you need to tweak the service invocation args, you can use the file `src/fts/oml/services/websockets/server/.env` for this purpose without restarting the service.

```yaml
websockets:
  build:
    context: ../websockets
    dockerfile: etc/fts/oml/services/websockets/dockerfile
    target: dev
  container_name: omlha-websockets
  depends_on:
    - redis-sentinel-1
    - redis-sentinel-2
    - redis-sentinel-3
  environment:
    - BIND_ADDRESS=0.0.0.0:8000
    - EVENT_LOOP=uvloop
    - LOGGERS=DEBUG:fts.oml.services.websockets;INFO:websockets.server
    - REDIS_HOST=redis+sentinel://redis/redis-sentinel-1,redis-sentinel-2,redis-sentinel-3
  image: freetechsolutions/oml-websockets:dev
  init: true
  ports:
    - 64310:8000/tcp
  tty: true
  volumes:
    - ../websockets/src:/opt/fts-oml-services-websockets/src
    - ../websockets/.envrc:/opt/fts-oml-services-websockets/.envrc
    - ../websockets/pyproject.toml:/opt/fts-oml-services-websockets/pyproject.toml
```

#### freetechsolutions/oml-websockets:run

Image for production run, basically just a python virtualenv with the package and dependencies installed.

You can use the following **docker run** invocation.

```sh
$ docker run \
  --detach \
  --name omlha-websockets-1 \
  --publish 8001:8000 \
  --restart=always \
  freetechsolutions/oml-websockets:prod
```

### centos7

We use systemd and podman for running the container as a system service.

1. Install podman (from _extras_ repository) and pull the service image.

```sh
$ yum -qy install podman
$ podman pull docker.io/freetechsolutions/oml-websockets:run
```

Integrate with systemd using one of the following approaches:

2. This approach is the one that is suported for podman version **1.6.4**. It requires that you create the container beforehand and the systemd service only starts and stop the existing container.

```sh
$ podman create \
  --env=VAR1=VAL1 \
  --name=oml-websockets-server \
  --publish=8000:8000/tcp \
  freetechsolutions/oml-websockets:run

$ podman generate systemd \
  --name \
  oml-websockets-server \
  > /etc/systemd/system/container-oml-websockets-server.service
```

3. This approach doesn't require an existing container and the systemd service creates/destroy the container. It is more flexible than the previous but require manual files creation since it is not supported by podman cli.

```sh
$ tee /etc/container-oml-websockets-server.env <<EOF
EVENT_LOOP=uvloop
LOGGERS=DEBUG:fts.oml.services.websockets,websockets.server
REDIS_HOST=redis+sentinel://redis/redis-sentinel-1,redis-sentinel-2,redis-sentinel-3
EOF

$ tee /etc/systemd/system/container-oml-websockets-server.service <<EOF
[Install]
WantedBy=multi-user.target

[Unit]
Description=container-oml-websockets-server

[Service]
ExecStart=/usr/bin/podman run \
  --env-file=/etc/container-oml-websockets-server.env \
  --name=oml-websockets-server \
  --publish=8000:8000/tcp \
  --rm \
  freetechsolutions/oml-websockets:run
ExecStop=/usr/bin/podman stop oml-websockets-server
Restart=on-failure
Type=simple
EOF
```

4. Starts the service

```sh
$ systemctl enable container-oml-websockets-server.service
$ systemctl start container-oml-websockets-server.service
```
