#!/bin/bash


# REDIS_HOST = redis+sentinel://master/redis-sentinel-1,redis-sentinel-2,redis-sentinel-3
SRC=/usr/src
COMPONENT_REPO=https://gitlab.com/omnileads/omnileads-websockets 

export oml_websocket_branch=oml-2207-dev-websockets-ha
export redis_host=192.168.95.120

echo "************************ disable SElinux *************************"
echo "************************ disable SElinux *************************" 

sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/sysconfig/selinux
sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
setenforce 0
systemctl disable firewalld > /dev/null 2>&1
systemctl stop firewalld > /dev/null 2>&1

yum -y install epel-release git python3 python3-pip libselinux-python3 podman

echo "************************ install ansible *************************"
echo "************************ install ansible *************************"
echo "************************ install ansible *************************"
pip3 install pip --upgrade
pip3 install 'ansible==2.9.2'
export PATH="$HOME/.local/bin/:$PATH"

echo "************************ clone REPO *************************"
echo "************************ clone REPO *************************"
cd $SRC
git clone $COMPONENT_REPO omlwebsocket
cd omlwebsocket
git checkout ${oml_websocket_branch}
cd etc/fts/services/websockets/

sed -i "s/omlredis_hostname=/omlredis_hostname=${redis_host}/g" ./inventory

# redis cluster sentinel
#sed -i "s/omlapp_02_hostname=/omlapp_02_hostname=${oml_app_02_host}/g" ./inventory
#sed -i "s/omlredis_01_hostname=/omlredis_01_hostname=${oml_redis_01_host}/g" ./inventory

ansible-playbook websocket.yml -i inventory