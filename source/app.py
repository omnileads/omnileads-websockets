# producer.py

# TODO: revisar uvloop por performance de event loop
import asyncio
import websockets
import aioredis

from subscription import SubscriptionManager

import logging

from conf import WEBSOCKET_PORT, LOG_LOCATION

logger = logging.getLogger("asyncio")

fh = logging.FileHandler(LOG_LOCATION + '/websockets.log')

logger.addHandler(fh)


async def browser_server(websocket, path):
    sm = SubscriptionManager(websocket, path)
    await sm.subscribe_to_redis()
    await sm.subscription_message()
    try:
        while True:
            await sm.get_data()

    except websockets.exceptions.ConnectionClosed:
        logger.info(f'Socket connection closed for url: {path}')
    except aioredis.errors.ConnectionClosedError:
        logger.info(f'Client connection closed for url: {path}')

    except Exception:
        logger.error("Fatal error in main loop", exc_info=True)
    finally:
        await sm.close_connnection()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    # loop.set_debug(True)
    ws_server = websockets.serve(browser_server, '0.0.0.0', WEBSOCKET_PORT)
    loop.run_until_complete(ws_server)
    loop.run_forever()
