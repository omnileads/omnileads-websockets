# syntax=docker/dockerfile:1.4

FROM python:3.10.4-slim-bullseye as dev
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y --no-install-recommends build-essential curl htop
RUN curl -sL -opkg.deb https://github.com/watchexec/watchexec/releases/download/cli-v1.17.1/watchexec-1.17.1-x86_64-unknown-linux-gnu.deb
RUN dpkg -i pkg.deb
RUN rm pkg.deb
RUN apt-get install -y --no-install-recommends direnv
RUN mkdir -p /root/.config/direnv
RUN printf > /root/.config/direnv/config.toml '%s\n' \
  '[whitelist]' \
  '  prefix = ["/opt/fts-oml-services-websockets"]'
ENV POETRY_HOME=/usr/local/poetry
RUN curl -sL https://install.python-poetry.org | python - && ln -s $POETRY_HOME/bin/poetry /usr/local/bin/poetry
WORKDIR /opt/fts-oml-services-websockets
COPY poetry.lock .
COPY poetry.toml .
COPY pyproject.toml .
RUN poetry env use python
RUN poetry install --no-root
COPY README.md .
COPY src src
RUN poetry install
ENTRYPOINT ["watchexec", "--restart", "--signal=SIGTERM", "--shell=none", "--watch=src", "--"]
CMD ["direnv", "exec", ".", "poetry", "run", "fts-oml-services-websockets-server"]



FROM python:3.10.4-slim-bullseye as run-build
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y --no-install-recommends build-essential curl
ENV POETRY_HOME=/usr/local/poetry
RUN curl -sL https://install.python-poetry.org | python - && ln -s $POETRY_HOME/bin/poetry /usr/local/bin/poetry
WORKDIR /opt/fts-oml-services-websockets
RUN \
  --mount=type=bind,source=poetry.lock,target=./poetry.lock \
  --mount=type=bind,source=poetry.toml,target=./poetry.toml \
  --mount=type=bind,source=pyproject.toml,target=./pyproject.toml \
  --mount=type=bind,source=README.md,target=./README.md \
  --mount=type=bind,source=src,target=./src \
  poetry env use python &&\
  poetry install --no-dev --no-root &&\
  poetry run pip install .



FROM python:3.10.4-slim-bullseye as run
RUN apt-get update
RUN apt-get upgrade -y
COPY --from=run-build /opt/fts-oml-services-websockets/.venv /opt/fts-oml-services-websockets/.venv
RUN ln -s /opt/fts-oml-services-websockets/.venv/bin/fts-oml-services-websockets-server /usr/local/bin/
CMD ["fts-oml-services-websockets-server"]
