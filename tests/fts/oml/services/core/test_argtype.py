from argparse import ArgumentTypeError
from urllib.parse import ParseResult
from urllib.parse import urlparse
from urllib.parse import urlunparse
from pytest import raises
from fts.oml.services.core.argtype import RedisHost
from fts.oml.services.core.argtype import RedisSentinel
from fts.oml.services.core.argtype import redis_host


def test_redis_host_instance():
  fixture = (
    "redis://localhost",
    "redis://localhost:6379",
  )
  for data in fixture:
    case = urlparse(data)
    redis = redis_host(data)
    assert isinstance(redis, RedisHost)
    assert redis.address.host == case.hostname
    assert redis.address.port == case.port if case.port is not None else 6379


def test_redis_host_instance_raises():
  with raises(ArgumentTypeError):
    redis_host("redis://")
    redis_host("redis://localhost:port")


def test_redis_host_sentinel():
  fixture = (
    "redis+sentinel://redis/redis-sentinel-1:26379,redis-sentinel-2:26379,redis-sentinel-3:26379",
    "redis+sentinel://redis:26379/redis-sentinel-1,redis-sentinel-2,redis-sentinel-3:26378",
  )
  for data in fixture:
    case = urlparse(data)
    redis = redis_host(data)
    assert isinstance(redis, RedisSentinel)
    assert redis.service == case.hostname
    for idx, part in enumerate(case.path.lstrip("/").split(",")):
      if ":" in part:
        host, port = part.split(":")
        port = int(port)
      else:
        host = part
        port = case.port or 26379
      assert redis.address[idx].host == host
      assert redis.address[idx].port == port


def test_redis_host_sentinel_raises():
  with raises(ArgumentTypeError):
    redis_host("redis+sentinel://")
    redis_host("redis+sentinel:///localhost:port")
