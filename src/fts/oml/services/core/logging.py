from functools import lru_cache
from logging import Logger
from logging import getLogger
from logging.config import dictConfig
from sys import stderr


def setup_logging(loggers: list[tuple[str, list[str]]]):
  dictConfig({
    "disable_existing_loggers": False,
    "formatters": {
      "default": {
        "format": "%(levelname).3s | %(name)s | %(message)s",
      },
    },
    "handlers": {
      "default": {
        "class": "logging.StreamHandler",
        "formatter": "default",
      },
    },
    "loggers": {},
    "root": {
      "handlers": ["default"],
      "level": "WARNING",
    },
    "version": 1
  })
  for level, names in loggers:
    for name in names:
      get_logger(name).setLevel(level)


def echo_debug_tree():
  try:
    from logging_tree.format import describe
    from logging_tree.nodes import tree
    stderr.write("\n")
    for line in describe(tree()):
      stderr.write(f"{line}\n")
    stderr.write("\n")
    stderr.flush()
  except ImportError:
    stderr.write("pip install logging_tree\n")


@lru_cache
def get_logger(name):
  return getLogger(name)
