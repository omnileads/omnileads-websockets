from os import getenv


def get(kind, key, default):
  return kind(getenv(key, default))
