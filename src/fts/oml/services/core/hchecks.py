from http import HTTPStatus
from json import dumps
from socket import gethostname
from typing import Any
from healthpy import pass_status
from healthpy import response_body


def default(checks: dict[str, Any], version: str):
  response = response_body(
    checks=checks,
    release_id=version,
    service_id=gethostname(),
    status=pass_status,
  )
  return (HTTPStatus.OK, (), dumps(response).encode())
