from argparse import ArgumentParser
from argparse import ArgumentTypeError
from re import compile
from typing import Any
from typing import Callable
from typing import NamedTuple
from urllib.parse import urlparse
from redis.asyncio import Redis
from redis.asyncio.sentinel import Sentinel
from . import environ


def add_argument(parser: ArgumentParser, flag: str, type: Callable[[str], Any], default: str):
  parser.add_argument(
    f"--{flag}",
    default=environ.get(
      type,
      flag.replace("-", "_").upper(),
      default,
    ),
    type=type,
  )


class Address(NamedTuple):
  host: str
  port: int


def address(value: str, regex=compile(r"(?P<host>\S+):(?P<port>\d+)")):
  if value:
    if regex.fullmatch(value):
      host, port = value.split(":")
      return Address(str(host), int(port))
    raise ArgumentTypeError(f"must be in the format {regex.pattern}")
  return None


def boolean(value: str, falsy=("0", "false", "no", "off"), truthy=("1", "true", "yes", "on")):
  if value:
    if value in truthy:
      return True
    if value in falsy:
      return False
    raise ArgumentTypeError(f"boolean value expected {falsy} or {truthy}")
  return None


def loggers(value: str) -> list[tuple[str, list[str]]]:
  if value:
    return [(
      logger.split(":", 1)[0],
      logger.split(":", 1)[1].split(","),
    ) for logger in value.split(";")]
  return []


def csvlist(value: str) -> list:
  if value:
    return value.split(",")
  return []


class RedisServer(NamedTuple):
  address: Address

  def client(self):
    return Redis(
      host=self.address.host,
      port=self.address.port,
      socket_timeout=10,
    )


class RedisSentinel(NamedTuple):
  address: list[Address]
  service: str

  def client(self):
    return Sentinel(
      [(address.host, address.port) for address in self.address],
      socket_timeout=10,
    ).slave_for(
      self.service,
    )


RedisHost = RedisServer | RedisSentinel


def redis_host(value: str) -> RedisHost | None:
  if value:
    url = urlparse(value)
    if url.hostname:
      if url.scheme == "redis":
        return RedisServer(Address(url.hostname, url.port or 6379))
      if url.scheme == "redis+sentinel":
        service = url.hostname
        defport = url.port or 26379
        address = []
        for part in url.path.lstrip("/").split(","):
          if ":" in part:
            host, port = part.split(":")
            port = int(port)
          else:
            host = part
            port = defport or 26379
          address.append(Address(host, port))
        return RedisSentinel(address, service)
    raise ArgumentTypeError("redisrc formats: redis://host[:port] or redis+sentinel://service[:port]/host[:port],...")
  return None
