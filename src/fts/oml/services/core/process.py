from sys import argv
from setproctitle import setproctitle


def fix_process_name(name=argv[0]):
  setproctitle(name)
