from argparse import ArgumentParser
from fts.oml.services.core.argtype import add_argument
from fts.oml.services.core.argtype import boolean
from fts.oml.services.core.argtype import csvlist
from fts.oml.services.core.logging import get_logger
from fts.oml.services.core.logging import setup_logging
from fts.oml.services.core.process import fix_process_name


def run():
  fix_process_name()
  parser = ArgumentParser()
  add_argument(parser, "debug", boolean, "on")
  add_argument(parser, "log-debug", csvlist, "")
  args = parser.parse_args()
  setup_logging(args.log_debug)
  logger = get_logger(__name__)
  logger.debug(f"Settings:")
  for k, v in vars(args).items():
    logger.debug(f" - {k}={v!r}")
