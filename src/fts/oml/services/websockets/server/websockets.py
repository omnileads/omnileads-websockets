from functools import partial
from fts.oml.services.core import hchecks
from fts.oml.services.core.argtype import Address
from fts.oml.services.core.argtype import RedisHost
from fts.oml.services.core.asyncio import CancelledError
from fts.oml.services.core.asyncio import Future
from fts.oml.services.core.asyncio import Loop
from fts.oml.services.core.asyncio import all_tasks
from websockets.server import WebSocketServerProtocol as WebSocket
from websockets.server import serve
from .. import __version__
from . import redis

websockets = set[WebSocket]()


async def server(bind_address: Address, redis_host: RedisHost, loop: Loop):
  async with serve(
    partial(websockets_handler, redis_host=redis_host, loop=loop),
    bind_address.host,
    bind_address.port,
    process_request=partial(requests_handler, redis_host=redis_host, loop=loop),
  ):
    try:
      await Future()
    except CancelledError:
      pass


async def requests_handler(path: str, headers, redis_host: RedisHost, loop: Loop):
  if path == "/health-checks":
    return hchecks.default({
      "tasks": get_tasks_status(loop),
      "redis:pubsubs": redis.get_pubsubs_status(),
      "redis:streams": redis.get_streams_status(),
      "websockets": get_websockets_status(),
    }, __version__)


async def websockets_handler(websocket: WebSocket, redis_host: RedisHost, loop: Loop):
  try:
    websockets.add(websocket)
    await redis.subscribe(websocket, redis_host, loop)
    await websocket.wait_closed()
  finally:
    await redis.unsubscribe(websocket)
    websockets.remove(websocket)


def get_tasks_status(loop: Loop):

  def task_order_key(task):
    name: str = task.get_name()
    if name.startswith("Task"):
      return "Task-{:0>8}".format(name[5:])
    return name

  tasks = sorted(all_tasks(loop), key=task_order_key)
  if loop.get_debug():
    return [str(task) for task in tasks]
  return [task.get_name() for task in tasks]


def get_websockets_status():
  return [{
    "id": str(ws.id),
    "path": ws.path,
    "remote_address": "{}:{}".format(*ws.remote_address),
  } for ws in websockets]
