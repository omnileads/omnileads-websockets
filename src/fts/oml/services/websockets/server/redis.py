from json import dumps
from redis.exceptions import TimeoutError
from fts.oml.services.core.argtype import RedisHost
from fts.oml.services.core.asyncio import CancelledError
from fts.oml.services.core.asyncio import Loop
from fts.oml.services.core.asyncio import Task
from fts.oml.services.core.asyncio import create_task
from fts.oml.services.core.asyncio import sleep
from fts.oml.services.core.logging import get_logger
from websockets.server import WebSocketServerProtocol as WebSocket

pubsubs = dict[WebSocket, Task]()

streams = dict[WebSocket, Task]()


async def connect_to_pubsub(name: str, websocket: WebSocket, redis_host: RedisHost):
  logger = get_logger(__name__)
  redis = redis_host.client()
  pubsub = redis.pubsub()
  await pubsub.subscribe(name)
  try:
    await websocket.send("Subscribed!")
    while websocket.open:
      try:
        msg = await pubsub.get_message(ignore_subscribe_messages=True, timeout=9.0)
        if msg:
          await websocket.send(msg.get("data").decode("utf-8"))
        # await sleep(1)
      except TimeoutError:
        pass
  except CancelledError:
    pass
  except Exception as exception:
    logger.error(f"Caught exception={exception!r}")
  finally:
    await pubsub.close()
    await redis.close(close_connection_pool=True)


async def connect_to_stream(name: str, websocket: WebSocket, redis_host: RedisHost):
  logger = get_logger(__name__)
  redis = redis_host.client()
  streams = {
    name: "0-0"
  }
  try:
    await websocket.send("Stream subscribed!")
    while websocket.open:
      try:
        payloads = []
        for stream, msgs in await redis.xread(streams=streams, block=1000 * 10):
          stream = stream.decode("utf-8")
          for msg_id, msg in msgs:
            # @audit legacy code start
            msg_id = msg_id.decode("utf-8")
            payload = list(msg.items())[0][1].decode("utf-8")
            stream_id_part = f'\'stream_id\':\'{msg_id}\''
            payload = f'{payload[:-1]},{stream_id_part}' + '}'
            # @audit legacy code ends
            payloads.append(payload)
          streams[stream] = msg_id
        await websocket.send(dumps(payloads))
        # await sleep(1)
      except TimeoutError:
        pass
  except CancelledError:
    pass
  except Exception as exception:
    logger.error(f"Caught exception={exception!r}")
  finally:
    await redis.close(close_connection_pool=True)


def get_pubsub_name(path: str):
  CHANNELS = {
    "genera_zip_grabaciones": "STATUS_DOWNLOAD",
    "genera_csv_auditoria": "STATUS_DOWNLOAD_AUDITORIA",
    "reporte_agente": "AGENT_REPORT",
    "reporte_grafico_campana": "STATUS_CSV_REPORT",
    "reporte_resultados_de_base_campana": "BASE_RESULTS_REPORT",
  }
  KEYS = {
    "calificados": "DISPOSITIONED",
    "contactados": "CONTACTED",
    "estadisticas_dia_actual": "CURRENT_DAY_STATS",
    "formulario_gestion": "ENGAGED_DISPOSITIONS",
    "grabaciones": "RECORDINGS",
    "no_atendidos": "NOT_ATTENDED",
    "resultados_de_base_contactados": "CONTACTED",
    "resultados_de_base_contactados_todos": "ALL_CONTACTED",
  }
  channel, key, report, task = path.strip("/").split("/")[1:]
  return "OML:{channel}:{key}:{report}:{task}".format(
    channel=CHANNELS[channel],
    key=KEYS[key],
    report=report,
    task=task,
  )


def get_stream_name(path: str):
  return path.rstrip("/").split("/stream/", 1)[1].replace("/", "_")


async def subscribe(websocket: WebSocket, redis_host: RedisHost, loop: Loop):
  if "/stream/" in websocket.path:
    cname = get_stream_name(websocket.path)
    tname = f"redis-stream id={websocket.id} name={cname}"
    streams[websocket] = create_task(loop, connect_to_stream(cname, websocket, redis_host), tname)
  else:
    cname = get_pubsub_name(websocket.path)
    tname = f"redis-pubsub id={websocket.id} name={cname}"
    pubsubs[websocket] = create_task(loop, connect_to_pubsub(cname, websocket, redis_host), tname)


async def unsubscribe(websocket: WebSocket):
  if "/stream/" in websocket.path:
    task = streams.pop(websocket)
  else:
    task = pubsubs.pop(websocket)
  task.cancel()
  del task


def get_pubsubs_status():
  return [task.get_name() for task in pubsubs.values()]


def get_streams_status():
  return [task.get_name() for task in streams.values()]
