from signal import SIGINT
from signal import Signals
from sys import stderr
from fts.oml.services.core.asyncio import Loop
from fts.oml.services.core.asyncio import all_tasks
from fts.oml.services.core.asyncio import create_task
from fts.oml.services.core.asyncio import current_task
from fts.oml.services.core.asyncio import gather
from fts.oml.services.core.logging import get_logger


def handler(loop: Loop, signal: Signals):
  if signal == SIGINT and stderr.writable():
    stderr.write("\b\b\r")
    stderr.flush()
  logger = get_logger(__name__)
  logger.info(f"Received exit signal={signal!r}")
  create_task(loop, shutdown_by_signal(loop), "shutdown-by-signal")


async def shutdown_by_signal(loop: Loop):
  logger = get_logger(__name__)
  logger.info("Shutting down")
  tasks = [task for task in all_tasks(loop) if task is not current_task(loop)]
  for task in tasks:
    task.cancel()
    logger.info(f"Cancelling outstanding task={task.get_name()!r}")
    if cr_name := getattr(task.get_coro(), "__qualname__", None):
      logger.debug(f" - coro-name='{cr_name}'")
    if cr_code := getattr(task.get_coro(), "cr_code", None):
      logger.debug(f" - coro-code='{cr_code.co_filename}:{cr_code.co_firstlineno}'")
  await gather(*tasks, return_exceptions=True)
  loop.stop()
