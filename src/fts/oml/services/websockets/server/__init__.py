from argparse import ArgumentParser
from functools import partial
from signal import SIGHUP
from signal import SIGINT
from signal import SIGTERM
from fts.oml.services.core.argtype import add_argument
from fts.oml.services.core.argtype import address
from fts.oml.services.core.argtype import loggers
from fts.oml.services.core.argtype import redis_host
from fts.oml.services.core.asyncio import get_event_loop
from fts.oml.services.core.logging import get_logger
from fts.oml.services.core.logging import setup_logging
from fts.oml.services.core.process import fix_process_name
from . import exceptions
from . import signals
from . import websockets


def run():
  fix_process_name()
  parser = ArgumentParser()
  add_argument(parser, "bind-address", address, "0.0.0.0:8000")
  add_argument(parser, "event-loop", str, "built-in")
  add_argument(parser, "loggers", loggers, "DEBUG:fts.oml.services.websockets;INFO:websockets.server")
  add_argument(parser, "redis-host", redis_host, "redis://redis:6379")
  args = parser.parse_args()
  setup_logging(args.loggers)
  logger = get_logger(__name__).getChild("args")
  for k, v in vars(args).items():
    logger.debug(f"< {k}={v!r}")
  if args.event_loop == "uvloop":
    from uvloop import install as uvloop_install
    uvloop_install()
  start(args.bind_address, args.redis_host)


def start(bind_address, redis_host):
  loop = get_event_loop()
  for signal in (SIGHUP, SIGINT, SIGTERM):
    loop.add_signal_handler(signal, partial(signals.handler, loop, signal))
  loop.set_exception_handler(exceptions.handler)
  try:
    loop.run_until_complete(websockets.server(bind_address, redis_host, loop))
  finally:
    loop.run_until_complete(loop.shutdown_asyncgens())
    loop.run_until_complete(loop.shutdown_default_executor())
    loop.close()
