from traceback import format_exception
from typing import Any
from fts.oml.services.core.asyncio import Loop
from fts.oml.services.core.asyncio import all_tasks
from fts.oml.services.core.asyncio import create_task
from fts.oml.services.core.asyncio import current_task
from fts.oml.services.core.asyncio import gather
from fts.oml.services.core.logging import get_logger


def handler(loop: Loop, context: dict[str, Any]):
  logger = get_logger(__name__)
  if exception := context.get("exception"):
    logger.error(f"Caught exception={exception!r}")
    for line in "".join(format_exception(exception)).splitlines():
      logger.error(f"  {line}")
  elif message := context.get("message"):
    logger.error(f"Caught error={message!r}")
  create_task(loop, shutdown_by_exception(loop), name="shutdown-by-exception")


async def shutdown_by_exception(loop: Loop):
  logger = get_logger(__name__)
  logger.info("Shutting down")
  tasks = [task for task in all_tasks(loop) if task is not current_task(loop)]
  for task in tasks:
    task.cancel()
    logger.debug(f"Cancelling outstanding task={task.get_name()!r}")
  await gather(*tasks, return_exceptions=True)
  loop.stop()
