#!/bin/bash
WEBSOCKETS_VERSION=$(cat ../../.websockets_version)

set -e
CODE_LOCATION="/opt/omnileads/websockets/"
BRANCH="$CI_COMMIT_REF_NAME"
echo "*** [oml-websockets] Installing python packages"
yum install python3-virtualenv -y
echo "*** [oml-websockets] Creating virtualenv dir"
virtualenv-3 $CODE_LOCATION -p python3.6
source $CODE_LOCATION/bin/activate
echo "*** [oml-websockets] Installing requirements packages"
cd /builds/omnileads/omnileads-websockets
pip3 install setuptools --upgrade
pip3 install -r source/requirements/base.txt
echo "*** [oml-websockets] Copying code files to $CODE_LOCATION"
cp source/*.py $CODE_LOCATION
echo "*** [oml-websockets] Building rpm"
fpm -s dir -t rpm -n omnileads-websockets -v $WEBSOCKETS_VERSION \
  --before-install build/rpm/scripts/before_install.sh \
  --after-install build/rpm/scripts/after_install.sh \
  --after-remove build/rpm/scripts/after_remove.sh \
  -f $CODE_LOCATION \
  build/rpm/omnileads-websockets.service=/etc/systemd/system/omnileads-websockets.service || true
mv omnileads-websockets-$WEBSOCKETS_VERSION* /root
echo "*** [oml-websockets] Uploading the rpm created to s3 bucket"
aws s3 cp /root/omnileads-websockets* s3://fts-public-packages/omnileads-websockets/omnileads-websockets-$WEBSOCKETS_VERSION.x86_64.rpm
