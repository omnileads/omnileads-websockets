#!/bin/bash
set -e
# Script that runs after websockets install
WEBSOCKETS_LOCATION="/opt/omnileads/websockets"
if [ ! -f $WEBSOCKETS_LOCATION/bin/python3 ]; then
  echo "Linking python3 binary to $WEBSOCKETS_LOCATION"
  ln -s /usr/bin/python3 $WEBSOCKETS_LOCATION/bin/python3
fi
chown -R omnileads. $WEBSOCKETS_LOCATION
echo "Restarting and enabling omnileads-websockets"
systemctl enable omnileads-websockets
systemctl restart omnileads-websockets
