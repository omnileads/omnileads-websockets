#!/bin/bash
# Script that runs after websockets remove
echo "Removing websockets symbolic links and folders"
rm -rf /opt/omnileads/websockets
