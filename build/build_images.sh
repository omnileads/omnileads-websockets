#!/bin/bash

WEBSOCKETS_VERSION=$(cat ../../.websockets_version)

docker login -u $DOCKER_USER -p $DOCKER_PASSWORD

if [ $CI_COMMIT_REF_NAME == "master" ]; then
  docker build -f Dockerfile -t freetechsolutions/omlwebsockets:latest ../..
  docker push freetechsolutions/omlwebsockets:latest
fi
docker build -f Dockerfile -t freetechsolutions/omlwebsockets:develop ../..
docker push freetechsolutions/omlwebsockets:develop
docker build -f Dockerfile -t freetechsolutions/omlwebsockets:$WEBSOCKETS_VERSION ../..
docker push freetechsolutions/omlwebsockets:$WEBSOCKETS_VERSION
